from tornado import web,ioloop
from tornado.options import define,options,add_parse_callback


from routes import handlers
import config

#定义环境参数  
define('port',default=config.PORT,type=int,help='set sever at')
define('debug',default=config.DEBUG,type=bool,help='debug mode is ')

if __name__ == '__main__':
    app = web.Application(handlers,debug=options.debug)
    app.listen(options.port)
    ioloop.IOLoop.current().start()
