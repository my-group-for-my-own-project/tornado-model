#!/usr/bin/env python
# encoding: utf-8
import datetime
import hmac
import os
import random
import string
import time
from typing import Any, Dict

import bcrypt


class ObjectDict(Dict[str, Any]):
    """Makes a dictionary behave like an object, with attribute-style access.
    """

    def __getattr__(self, name: str) -> Any:
        try:
            return self[name]
        except KeyError:
            raise AttributeError(name)

    def __setattr__(self, name: str, value: Any) -> None:
        self[name] = value


def gen_format_date(pattern='%Y-%m-%d %H:%M:%S', timestamp=None):
    """
    生成格式化的日期字符串
    :param pattern: 日期格式
    :param timestamp: 要转换的日期，默认为当前时间
    :return:
    """
    if not timestamp:
        date_obj = datetime.datetime.now()
    else:
        date_obj = timestamp
    return date_obj.strftime(pattern)

def strptime(time_str, pattern="%Y-%m-%d %H:%M:%S"):
    """
    字符串转变为日期类型
    :param time_str:
    :param pattern:
    :return:
    """
    return datetime.datetime.strptime(time_str, pattern)


def generate_random_string(length, with_punctuation=True, with_digit=True):
    """
    Linux正则命名参考：http://vbird.dic.ksu.edu.tw/linux_basic/0330regularex.php#lang
    [:alnum:]	代表英文大小写字节及数字，亦即 0-9, A-Z, a-z
    [:alpha:]	代表任何英文大小写字节，亦即 A-Z, a-z
    [:blank:]	代表空白键与 [Tab] 按键两者
    [:cntrl:]	代表键盘上面的控制按键，亦即包括 CR, LF, Tab, Del.. 等等
    [:digit:]	代表数字而已，亦即 0-9
    [:graph:]	除了空白字节 (空白键与 [Tab] 按键) 外的其他所有按键
    [:lower:]	代表小写字节，亦即 a-z
    [:print:]	代表任何可以被列印出来的字节
    [:punct:]	代表标点符号 (punctuation symbol)，亦即：" ' ? ! ; : # $...
    [:upper:]	代表大写字节，亦即 A-Z
    [:space:]	任何会产生空白的字节，包括空白键, [Tab], CR 等等
    [:xdigit:]	代表 16 进位的数字类型，因此包括： 0-9, A-F, a-f 的数字与字节

    Python自带常量(本例中改用这个，不用手工定义了)
    string.digits		  #十进制数字：0123456789
    string.octdigits	   #八进制数字：01234567
    string.hexdigits	   #十六进制数字：0123456789abcdefABCDEF
    string.ascii_lowercase #小写字母(ASCII)：abcdefghijklmnopqrstuvwxyz
    string.ascii_uppercase #大写字母(ASCII)：ABCDEFGHIJKLMNOPQRSTUVWXYZ
    string.ascii_letters   #字母：(ASCII)abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ
    string.punctuation	 #标点符号：!"#$%&'()*+,-./:;<=>?@[\]^_`{|}~

    以下的不用，有locale问题
    string.lowercase	   #abcdefghijklmnopqrstuvwxyz
    string.uppercase	   #ABCDEFGHIJKLMNOPQRSTUVWXYZ
    string.letters		 #ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz

    以下的不能用
    string.whitespace	  #On most systems this includes the characters space, tab, linefeed, return, formfeed, and vertical tab.
    string.printable	   #digits, letters, punctuation, and whitespace
    """
    punctuation = '!$%^&*'
    random.seed(str(time.time()) + str(os.urandom(32)))

    rdw_seed = string.ascii_letters

    if with_digit:
        rdw_seed += string.digits

    if with_punctuation:
        rdw_seed += punctuation

    rdw = []
    while len(rdw) < length:
        rdw.append(random.choice(rdw_seed))
    return ''.join(rdw)


def create_guid(prefix=None, guid_len=12, with_punctuation=False, islower=True):
    gen = generate_random_string(guid_len, with_punctuation)
    out_str = '%s_%s' % (prefix, gen) if prefix else prefix
    return out_str.lower() if islower else out_str.upper()

def encryp_password(password):
    """密码加密"""
    hashed = bcrypt.hashpw(password.encode('utf-8'), bcrypt.gensalt())
    return hashed

def verify_password(password, hashed):
    """密码验证"""
    # Validating a hash (don't use ==)
    if (hmac.compare_digest(bcrypt.hashpw(password, hashed), hashed)):
        # Login successful
        return True
    else:
        return False



