#!/usr/bin/env python
# encoding: utf-8
import asyncio
import json
import logging
import traceback

from config import MYSQL_DB
import aiomysql

loop = asyncio.get_event_loop()


class ObjectDict(dict):
    """Makes a dictionary behave like an object, with attribute-style access.
    """

    def __getattr__(self, name):
        # type: (str) -> any
        try:
            return self[name]
        except KeyError:
            raise AttributeError(name)

    def __setattr__(self, name, value):
        # type: (str, any) -> None
        self[name] = value


class NoResultError(Exception):
    pass


async def create_db_pool(host, port, user, password, database):
    return await aiomysql.create_pool(
        host=host,
        port=port,
        user=user,
        password=password,
        db=database,
        autocommit=True,
        minsize=1,
        maxsize=10,
        loop=loop
    )


mysql_pool = loop.run_until_complete(create_db_pool(**MYSQL_DB))


class DBModel(object):
    def __init__(self, db_pool):
        self.db_pool = db_pool
        self.transaction = []

    async def find(self, table, fields='*', condition=None):
        """
        query one
        :param table:
        :param fields:
        :param condition:
        :return:
        """
        condition, params = self._dict_to_where_str(condition)
        sql_str = f'SELECT {fields} FROM {table} {condition}'
        rs = await self.queryone(sql_str, *params)
        return rs

    async def select(self, table, fields='*', condition=None, order=None, limit=0, offset=0):
        """
        query all
        :param table:
        :param fields:
        :param condition:
        :param order:
        :param limit:
        :param offset:
        :return:
        """
        condition, params = self._dict_to_where_str(condition)
        sql_str = f'SELECT {fields} FROM {table} {condition}'
        if order:
            sql_str += ' ORDER BY ' + ','.join(map(lambda x: x[1:] + ' DESC' if x[0] == '-' else x + ' ASC', order))
        if limit:
            sql_str += f' LIMIT {offset}, {limit}'

        rs = await self.query(sql_str, *params)
        return rs

    async def insert(self, table, data_dict, return_id=False):
        """
        插入数据
        :param data_dict: 插入的数据
        :return:
        """
        sql_str = f'INSERT INTO {table}'
        params, fields, values = [], [], []
        for _key, _val in data_dict.items():
            fields.append(_key)
            values.append('%s')
            params.append(_val)
        fields_str = ','.join(fields)
        values_str = ",".join(values)
        sql_str += "({0}) VALUES ({1})".format(fields_str, values_str)
        if not return_id:
            rs = await self.execute(sql_str, *params)
        else:
            await self.execute(sql_str, *params)
            rs = await self.queryone("select last_insert_id() id")
        return rs

    async def insert_many(self, table, data_list):
        """
        插入多条数据
        :param data_dict: 插入的数据
        :return:
        """
        params, fields, values = [], [], []
        for _key, _val in data_list[0].items():
            fields.append(_key)
        fields_str = ','.join(fields)
        sql_str = f'INSERT INTO {table} ({fields_str}) VALUES '
        for index, row in enumerate(data_list):
            value_str = ','.join("'%s'" % id for id in row.values())
            sql_str += f"({value_str})"
            if index < len(data_list) - 1:
                sql_str += ","

        rs = await self.execute(sql_str)
        return rs

    def trans_insert(self, table, data_dict):
        sql_str = f'INSERT INTO {table}'
        params, fields, values = [], [], []
        for _key, _val in data_dict.items():
            fields.append(_key)
            values.append('%s')
            params.append(_val)
        fields_str = ','.join(fields)
        values_str = ",".join(values)
        sql_str += "({0}) VALUES ({1})".format(fields_str, values_str)
        self.transaction.append([sql_str, params])

    async def update(self, table, data_dict, condition=None):
        """
        修改语句
        :param data_dict:
        :param condition:
        :return:
        """
        sql_str = f'UPDATE {table} SET '
        fields, params = [], []
        for k, v in data_dict.items():
            fields.append(f'{k}=%s')
            params.append(v)

        sql_str += ",".join(fields)

        condition, query_params = self._dict_to_where_str(condition)
        sql_str += f' {condition}'
        params.extend(query_params)

        rs = await self.execute(sql_str, *params)
        return rs

    def trans_update(self, table, data_dict, condition=None):
        sql_str = f'UPDATE {table} SET '
        fields, params = [], []
        for k, v in data_dict.items():
            fields.append(f'{k}=%s')
            params.append(v)

        sql_str += ",".join(fields)

        condition, query_params = self._dict_to_where_str(condition)
        sql_str += f' {condition}'
        params.extend(query_params)
        self.transaction.append([sql_str, params])

    async def count(self, table, condition=None, params=None):
        """统计条数"""
        condition, query_params = self._dict_to_where_str(condition)
        if not params:
            params = query_params
        count_str = "SELECT count(1) as num FROM {0} {1}".format(table, condition)
        res = await self.queryone(count_str, *params)
        if res:
            rv = res['num']
        else:
            rv = 0
        return rv

    def _dict_to_where_str(self, condition_dict):
        return_str, params = '', []
        if condition_dict:
            sqls = []
            for _key, _val in condition_dict.items():
                if isinstance(_val, tuple):
                    sqls.append(f'{_key} {_val[0]} %s')
                    params.append(_val[1])
                elif isinstance(_val, list):
                    db_array = self._list_to_db_array(_val)
                    sqls.append(f'{_key} in {db_array}')
                elif _val is None:
                    sqls.append(f'{_key} is null')
                else:
                    sqls.append(f'{_key}=%s')
                    params.append(_val)

            return_str = 'WHERE ' + ' AND '.join(sqls)

        return return_str, params

    def _list_to_db_array(self, list_data):
        list_str = json.dumps(list_data)
        return list_str.replace('[', '(').replace(']', ')').replace('"', "'")


class AioMySQL(DBModel):
    def echo(self, stmt, args=()):
        """Print SQL statement"""
        args = [f"'{row}'" if type(row) == str else row for row in args]
        out_sql = stmt % tuple(args) if args else stmt
        return out_sql

    async def get_cursor(self):
        conn = await self.db_pool.acquire()
        # 返回字典格式
        cur = await conn.cursor(aiomysql.DictCursor)
        return conn, cur

    async def execute(self, stmt, *args):
        """Execute a SQL statement.

        Must be called with ``await self.execute(...)``
        """
        conn, cur = await self.get_cursor()
        try:
            await cur.execute(stmt, args)
            if cur.rowcount == 0:
                return False
            else:
                return True
        except:
            logging.error(traceback.format_exc())
        finally:
            logging.info(self.echo(stmt, args))
            if cur:
                await cur.close()
            await self.db_pool.release(conn)

    async def query(self, stmt, *args):
        """Query for a list of results.
        """
        conn, cur = await self.get_cursor()
        try:
            await cur.execute(stmt, args)
            return await cur.fetchall()
        except Exception as e:
            logging.error(traceback.format_exc())
        finally:
            logging.info(self.echo(stmt, args))
            if cur:
                await cur.close()
            await self.db_pool.release(conn)

    async def queryone(self, stmt, *args):
        """Query for exactly one result.

        Raises NoResultError if there are no results, or ValueError if
        there are more than one.
        """
        conn, cur = await self.get_cursor()
        try:
            await cur.execute(stmt, args)
            return await cur.fetchone()
        except Exception as e:
            logging.error(traceback.format_exc())
        finally:
            logging.info(self.echo(stmt, args))
            if cur:
                await cur.close()
            # 释放掉conn,将连接放回到连接池中
            await self.db_pool.release(conn)


aiomysql_db = AioMySQL(mysql_pool)

if __name__ == '__main__':
    # print(aiomysql_db)
    async def show_version():
        res = await aiomysql_db.find('users')
        print(res)


    loop.run_until_complete(show_version())
