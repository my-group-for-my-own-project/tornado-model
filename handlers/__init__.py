import json

from tornado import web
class BaseHandler(web.RequestHandler):
   
   def set_default_headers(self):
        self.set_header("Access-Control-Allow-Origin", "*")
        self.set_header("Access-Control-Allow-Headers", "Content-Type")
        self.set_header("Access-Control-Allow-Methods", "POST,GET,OPTIONS")

   def options(self):
        # print(self)
        self.set_status(204)
        self.finish()

   def write_json(self, code, data=None):
        """返回json数据时用,默认正常时状态为100"""
        ejson = {'code': code}
        # if code != 100:
        #     if data:
        #         ejson['msg'] = data
        #         self.log.warning("[API error]code => {1},msg => {2}".format(self.request.uri, code, data))
        # else:
        #     ejson['data'] = data
        ejson['data'] = data
        jsonstr = json.dumps(ejson)
        self.write(jsonstr)
        self.finish()

   def get_param(self, argument, argument_default=None):
        """获取请求参数"""
        if self.request.headers.get("Content-type") == 'application/json;charset=UTF-8':
            paramrs = self.request.body
            if paramrs:
                paramrs_dict = json.loads(paramrs)
                _param = paramrs_dict.get(argument, argument_default)
            else:
                _param = argument_default
        else:

            _param = self.get_argument(argument, argument_default)
            if not _param:
                _param = argument_default

        return _param
